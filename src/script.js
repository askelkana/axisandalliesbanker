var $ = require('jquery');
var sprintf = require('sprintf-js').sprintf;

var $player;
var purchase = {};
var combat = {};

$(function () {
  findDOM();
  addEventListeners();
  selectNextPlayer();
});

function $getPlayer(player) {
  if (player) {
    return $player.parent().children(sprintf('[data-player="%s"]', player));
  }
  return $player;
}

function addEventListeners() {
  onPurchaseUnit();
  onCompletePurchase();
  onClearPurchases();
  onOccupyTerritory();
  onFinaliseTurn();
  onAdjustICUs();
  onBackToPurchaseView();

  function onPurchaseUnit() {
    $('.unit')
      .on('click', '.more:not(.disabled)', function (e) {
        var $this = $(e.target);
        var $total = $this.prev();
        var cost = $this.parent().next().attr('data-cost') * 1;
        var total = $total.attr('data-total') * 1;
        $total.attr('data-total', ++total);
        adjustCost(cost);
      })
      .on('click', '.less:not(.disabled)', function (e) {
        var $this = $(e.target);
        var $total = $this.next();
        var cost = $this.parent().next().attr('data-cost') * 1;
        var total = $total.attr('data-total') * 1;
        $total.attr('data-total', --total);
        adjustCost(-cost);
      });
  }

  function onCompletePurchase() {
    $('#purchase').on('click', function () {
      moveToCombatPhase();
    });
  }

  function onClearPurchases() {
    $('#clear').on('click', clearPurchases);
  }

  function onOccupyTerritory() {
    $('#territories').on('click', '> div', function (e) {
      $(e.target).toggleClass('occupied');
    });
  }

  function onFinaliseTurn() {
    $('#finalise-turn').on('click', function () {
      accrueICUs();
      if (!$player.attr('data-capital-occupied')) {
        accrueCredit();
      }
      selectNextPlayer();
    });
  }

  function onBackToPurchaseView() {
    $('#back-to-purchase').on('click', function () {
      moveToPurchasePhase(true);
    });
  }

  function onAdjustICUs() {
    $('.icu')
      .on('click', '.more', function (e) {
        var player = getPlayer(e);
        adjustICUs(1, player);
        $(e.target).siblings('.less').removeClass('disabled');
      })
      .on('click', '.less:not(.disabled)', function (e) {
        var player = getPlayer(e);
        $(e.target).toggleClass('disabled', !adjustICUs(-1, player));
      });

    function getPlayer(e) {
      return $(e.target).closest('[data-player]').attr('data-player');
    }
  }
}

function accrueCredit() {
  var icu = getICUs();
  adjustCredit(icu);
}

function accrueICUs() {
  combat.$territories.filter('.occupied').each(function () {
    var $territory = $(this);
    var originalOwner = $territory.attr('data-original-owner');
    var $originalOwner = $getPlayer(originalOwner);
    var originalOwnerTeam = $territory.attr('data-original-owner-team');
    var defendingPlayer = $territory.attr('data-occupier') || originalOwner;
    var attackingPlayer = $player.attr('data-player');
    var attackingPlayerTeam = $player.attr('data-team');
    var territoryICUs = $territory.attr('data-icu') * 1;
    var territoryHasCapital = $territory.attr('data-capital');
    if (originalOwnerTeam !== attackingPlayerTeam) {
      occupyTerritory();
    } else {
      liberateTerritory();
    }

    function occupyTerritory(friendly) {
      $territory.attr('data-occupier', attackingPlayer);
      $territory.attr('data-occupier-team', attackingPlayerTeam);
      adjustICUs(territoryICUs);
      if (!friendly) {
        adjustICUs(-territoryICUs, defendingPlayer);
        if (territoryHasCapital) {
          captureCapital();
        }
      }

      function captureCapital() {
        var booty = getCredit(originalOwner);
        adjustCredit(-booty, originalOwner);
        adjustCredit(booty, attackingPlayer);
        $originalOwner.attr('data-capital-occupied', true);
      }
    }

    function liberateTerritory() {
      var ownersCapitalIsOccupied = $originalOwner.attr('data-capital-occupied');
      if (ownersCapitalIsOccupied) {
        occupyTerritory(true);
      } else {
        restoreTerritoryToOwner($territory);
      }
      if (territoryHasCapital) {
        liberateCapital();
        restoreICUsOfFriendlyOccupiedTerritories();
      }

      function liberateCapital() {
        $originalOwner.removeAttr('data-capital-occupied');
      }

      function restoreICUsOfFriendlyOccupiedTerritories() {
        combat.$territories
          .filter(sprintf('[data-original-owner="%s"]', originalOwner))
          .filter(sprintf('[data-occupier-team="%s"]', originalOwnerTeam)).each(function () {
          var $territory = $(this);
          restoreTerritoryToOwner($territory);
        });
      }

      function restoreTerritoryToOwner($territory) {
        var territoryICUs = $territory.attr('data-icu') * 1;
        var occupier = $territory.attr('data-occupier');
        $territory.removeAttr('data-occupier');
        $territory.removeAttr('data-occupier-team');
        adjustICUs(territoryICUs, originalOwner);
        adjustICUs(-territoryICUs, occupier);
      }
    }
  });
}

function adjustCost(cost) {
  var totalCost = purchase.$cost.attr('data-cost') * 1;
  totalCost += cost;
  purchase.$cost.attr('data-cost', totalCost);
  setPurchaseButtons(totalCost);
}

function adjustCredit(sum, player) {
  var $player = $getPlayer(player);
  var $credit = $player.children('.credit');
  var credit = $credit.attr('data-credit') * 1;
  $credit.attr('data-credit', credit + sum);
}

function adjustICUs(sum, player) {
  var $player = $getPlayer(player);
  var $icu = $player.find('.total');
  var icu = $icu.attr('data-total') * 1;
  icu += sum;
  $icu.attr('data-total', icu);
  return icu;
}

function clearPurchases() {
  purchase.$cost.attr('data-cost', 0);
  setPurchaseButtons(0);
}

function findDOM() {
  $player = $('#players').children('.selected');
  purchase.$panel = $('#purchase-units');
  purchase.$units = purchase.$panel.find('.unit');
  purchase.$cost = $('#cost');
  purchase.$clear = $('#clear');
  combat.$panel = $('#combat-moves');
  combat.$territories = combat.$panel.find('#territories > div');
}

function getCredit(player) {
  var $player = $getPlayer(player);
  var $credit = $player.children('.credit');
  return $credit.attr('data-credit') * 1;
}

function getICUs(player) {
  var $player = $getPlayer(player);
  return $player.find('.total').attr('data-total') * 1;
}

function mobiliseUnits() {
  var cost = purchase.$cost.attr('data-cost') * 1;
  adjustCredit(-cost);
}

function moveToCombatPhase() {
  mobiliseUnits();
  toggleView(purchase, false);
  combat.$territories.removeClass('occupied');
  hideFriendlyTerritories();
  toggleView(combat, true);

  function hideFriendlyTerritories() {
    var $territories = combat.$territories;
    var currentPlayerTeam = $player.attr('data-team');
    var $friendlyOccupiedTerritories = $territories.filter(sprintf('[data-occupier-team="%s"]', currentPlayerTeam));
    var $friendlyUnoccupiedTerritories = $territories
      .filter(sprintf('[data-original-owner-team="%s"]', currentPlayerTeam))
      .filter(':not([data-occupier-team])');
    $territories.removeClass('hidden');
    $friendlyOccupiedTerritories.add($friendlyUnoccupiedTerritories).addClass('hidden');
  }
}

function moveToPurchasePhase(demob) {
  toggleView(combat, false);
  if (demob) {
    demobiliseUnits();
  } else {
    resetView();
  }
  toggleView(purchase, true);

  function resetView() {
    purchase.$panel.find('.total').attr('data-total', 0);
    purchase.$cost.attr('data-cost', 0);
    setPurchaseButtons(0);
  }
}

function demobiliseUnits() {
  var spent = purchase.$cost.attr('data-cost') * 1;
  adjustCredit(spent);
}

function setPurchaseButtons(cost) {
  var resetTotals = !cost;
  var limit = getCredit() - cost;
  purchase.$panel.find('.tally').each(function () {
    var $this = $(this);
    var $unit = $this.parent();
    var $parts = $this.children();
    var cost = $this.next().attr('data-cost') * 1;
    var total;
    var tooExpensive = cost > limit;
    if (resetTotals) {
      $parts.eq(1).attr('data-total', 0);
    }
    total = $parts.eq(1).attr('data-total') * 1;
    $parts.eq(0).toggleClass('disabled', !total);
    $parts.eq(2).toggleClass('disabled', tooExpensive);
    $unit
      .toggleClass('purchased', !!total)
      .toggleClass('too-expensive', tooExpensive);
  });
}

function selectNextPlayer() {
  var $sovietUnion = $player.siblings().first();
  $player.removeClass('selected');
  $player = $player.next();
  if ($player.length === 0) {
    $player = $sovietUnion;
  }
  $player.addClass('selected');
  moveToPurchasePhase();
}

function toggleView(view, state) {
 view.$panel.toggleClass('selected', state);
}
