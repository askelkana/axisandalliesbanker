module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);
  grunt.initConfig({
    browserify: {
      dist: {
        src:  [ 'src/script.js' ],
        dest: 'script.js'
      }
    },
    less:       {
      dist: {
        files: {
          'styles.css': 'src/styles.less'
        }
      }
    },
    watch:      {
      options: {
        spawn: false
      },
      less:    {
        files: [ 'src/*.less' ],
        tasks: [ 'less' ]
      },
      js:      {
        files: [ 'src/*.js' ],
        tasks: [ 'browserify' ]
      }
    }
  });

  // Default task.
  grunt.registerTask('default', [ 'watch' ]);
};
